PREFIX		=	arm-eabi-
CC		=	$(PREFIX)gcc
LD		=	$(PREFIX)ld
OBJCOPY		=	$(PREFIX)objcopy
OBJECTS		=	asick72.o

# Tables are in tables/
TABLE		=	cavepgm
OUTPUT		=	asick72.bin
ALTNAMES	=	ket_igs027a.bin ddp3_igs027a.bin espgal_igs027a.bin

ARCH		=	-mcpu=arm710t
CFLAGS		=	$(ARCH) -c -nostdlib -e 0x00000000
SFLAGS		=	$(ARCH)
LDFLAGS		=	-e 0x00000000 -nostdlib
OBJECTS		+=	tables/$(TABLE).o
CFLAGS		+=	-DCOMMAND_TABLE_NAME=$(TABLE)_cmd_table

.PHONY: all clean release

all: ket ddp3 espgal
ket: $(OUTPUT)
	cp $(OUTPUT) ket_igs027a.bin
ddp3: $(OUTPUT)
	cp $(OUTPUT) ddp3_igs027a.bin
espgal: $(OUTPUT)
	cp $(OUTPUT) espgal_igs027a.bin

%.o: %.S
	$(CC) $(SFLAGS) $(CFLAGS) -o $@ $<
%.o: %.s
	$(CC) $(SFLAGS) -o $@ $<
%.bin: $(OBJECTS)
	$(LD) $(LDFLAGS) -o $@ $(OBJECTS)
	$(OBJCOPY) -O binary $@ $@.bin
	dd if=$@.bin of=$@ bs=4096 count=1 conv=sync >/dev/null 2>&1
	$(RM) $@.bin
release:
	$(RM) $(OBJECTS)
clean: release
	$(RM) $(OUTPUT) $(ALTNAMES)

