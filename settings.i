/* CAVE PGM ASIC027A replacement ROM: ASick72
 * 
 * Copyright (C) 2010 Alex Marshall "trap15" <trap15@raidenii.net>
 * Copyright (C) 2010 "austere" <austere@raidenii.net>
 * Copyright (C) 2010 "nimitz"
 */

/* This is the number of "false returns" the command handler returns before
 * actually processing the request. The higher the value, the slower the game
 * will run. It is presumed that the official CAVE ASIC ROM utilizes a method
 * similar to this for assisting in game speed control. The actual value of this
 * is determined by STALL_TYPE as well.
 */
#define STALL_LENGTH		0
/* This is the type of stalling that will occur. Read further for the type
 * definitions.
 */
#define STALL_TYPE		(STALL_TYPE_DIRECT)

/* Inverse stall type means 1 stall will be issued per STALL_LENGTH returns. */
#define STALL_TYPE_INVERSE	(0)
/* Direct stall type means STALL_LENGTH stalls will be issued per return. */
#define STALL_TYPE_DIRECT	(1)

